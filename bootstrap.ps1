. { iwr -useb https://boxstarter.org/bootstrapper.ps1 } | iex
get-boxstarter -Force

$secpasswd = ConvertTo-SecureString "q" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential ("admin", $secpasswd)
Install-BoxstarterPackage -PackageName "https://bitbucket.org/davidfromdenmark/boxstarter/raw/c03e989f42ad8bb9f92762ffc3d34f6a58fb212d/base-box.ps1" -Credential $cred